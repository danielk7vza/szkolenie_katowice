// const info = document.getElementById("info")
// info.innerHTML = "za mało"

//Biblioteka Math -> Zbiory matematycznych funckji
// Math.floor() Zaokrąglenie liczby w dół
// Math.random() -> pseudolosowe liczby
var wylosowana = Math.floor(Math.random()*100) + 1
var counter = 0
// Sprawdzamy czy próba zgadnięcia jest mniejsza od 5
// wykorzustujemy do tego operator &&
function zgaduj()
{
    // Scope zmiennej -> zasięg 
    // var = let -> mniejszy zasięg zmiennej
    let liczba = document.getElementById("number").value // Pobranie wartości
    if(liczba == wylosowana && counter < 5) {
        alert('Wygrałeś liczba to: ' + wylosowana)
    }
    if(liczba < wylosowana) {
        alert('Podałes za małą liczbe')
    }
    if(liczba > wylosowana ) {
        alert('Podałes za dużą liczbę')
    }
}

var x = 1;
var y = 2;

// || lub && i
if(x > 1 || x % 2 == 0) {
    alert('jest wieksze od jeden lub jest parzysta')
}
if(x > 1 && x % 2 == 0) {
    alert('Jest wieksze od 1 oraz jest liczba parzysta')
}

function register() {
    let name = document.getElementById("name").value // Pobranie wartości
    let surname = document.getElementById("surname").value // Pobranie wartości
    let age = document.getElementById("age").value // Pobranie wartości

    if(name.length <= 0) {
        alert('Uzupełnij wszystkie pola')
    }
    if(surname.length <= 0) {
        alert('Uzupełnij wszystkie pola')
    }
    if(age.length <= 0) {
        alert('Uzupełnij wszystkie pola')
    }
}

function check() {
    let name = document.getElementById("name").value
    if(name.length <= 0) {
        alert('Przoszę uzupełnić wszystkie pola')
    }
}