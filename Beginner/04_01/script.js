// Najprostsza deklaracja funckji
function dodawanie() {
    var a = 1  
    var b = 2
    var suma = a + b;
    console.log(suma)
}
// Wywołanie funkcji
dodawanie()
var x
// Funkcje z parametrem
function add(x,y) {
    var suma = x + y
    console.log(suma)
}
// scope -> zakres zmiennej
function sub(x,y) {
    var suma = x - y
    console.log(suma)
}
add(10,25)
sub(10,25)

// Funkcja zwrotna
function multiply(x,y) {
    return x*y
}
console.log(zmienna)
// Funkcja która wywołuje się natychmiast
// Po znalezieniu danego kodu w przeglądarce
(function() {
    var x = 1
    var y = 2
    console.log(x*y)
})



function bigger(x,y) {
    if(x > y) {
        return 'x jest wieksze'
    } else {
        return 'y jest wieksze'
    }
}
var zmienna = bigger(10,5)
// funckje ktora pobiera 1 parametr x
// oraz zwraca czy liczba jest parzysta czy nie
// wynik funkcji zapisujecie do zmiennej
// i console.log(zmienna) -> pokazuje wynik
// console.log(zmienna) 'Tak jest parzysta'




function findBigger() {
    a > b ? console.log("a: jest większe", a) : console.log("b: Jest większe", b)
}

findBiggestFraction()

// Funkcja wywoływana na samym początku programu -> tak szybko jak przeglądrka odczyta plik
(function() {
    var result = 12 / 0.75;
    console.log("12 divided by 0.75 is ", result);
}())
